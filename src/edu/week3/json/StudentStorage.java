package edu.week3.json;


import java.util.*;

public class StudentStorage {

    public static void main(String[] args) {

        //Introduction message and instructions
        System.out.println("This student storage program allows the teacher to enter important information ");
        System.out.println("about their students and store it in JSON format in the file Student_Record.txt");
        System.out.println("it will also display any records saved to the file.");
        System.out.println();
        System.out.println("The program stores: Student Name, Parent Name, Student ID, Teacher Name & Grade");
        System.out.println();

        // asks the user if they would like to add a new student to the record.
        Database database = new Database();
        System.out.println("Would you like to add a new students information to the file 'Y' or 'N'?");
        Scanner addStudentInput = new Scanner(System.in);
        String addStudent = addStudentInput.next();
        System.out.println();
        if (addStudent.charAt(0) == 'y' || addStudent.charAt(0) == 'Y') {
            database.addStudent();
        }

        // displays the JSON records in the file "Student_Record.txt" if it exists and the format is correct.
        database.displayRecord();

        //Goodbye
        System.out.println("Thank you for using the student storage program.");
    }
}

