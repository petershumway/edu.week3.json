package edu.week3.json;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;



public class Database {
    // Data member variables
    private List studentList;
    private List saveFileList;
    private File save;

    // Member methods
    Database() {
        this.studentList = new ArrayList();
        this.saveFileList = new ArrayList();
    }

    void addStudent() {
        String addStudent = "Y";
        Scanner addStudentInput = new Scanner(System.in);

        // if user enters Y then prompt for student informaton
        while (addStudent.charAt(0) == 'y' || addStudent.charAt(0) == 'Y') {
            addStudent = "N";
            Student newStudent = new Student();

            System.out.println("Please enter the following details for the new student:");

            newStudent.setName(getStringInput("Student Name"));
            newStudent.setParentName(getStringInput("Parent Name"));
            newStudent.setStudentId(getIdInput());
            newStudent.setTeacher(getStringInput("Teacher Name"));

            // identify the grade given and correct it to the best format.
            Boolean gradeIncorrect = true;
            while (gradeIncorrect) {
                String grade = getStringInput("Student Grade");

                switch (grade.charAt(0)) {
                    case 'a':
                    case 'A':
                        newStudent.setGrade('A');
                        gradeIncorrect = false;
                        break;

                    case 'b':
                    case 'B':
                        newStudent.setGrade('B');
                        gradeIncorrect = false;
                        break;

                    case 'c':
                    case 'C':
                        newStudent.setGrade('C');
                        gradeIncorrect = false;
                        break;

                    case 'd':
                    case 'D':
                        newStudent.setGrade('D');
                        gradeIncorrect = false;
                        break;

                    case 'f':
                    case 'F':
                        newStudent.setGrade('F');
                        gradeIncorrect = false;
                        break;

                    default:
                        // error for any other invalid input
                        System.out.println("Error: Valid letter grade entries are A, B, C, D & F\n");
                        gradeIncorrect = true;
                        break;
                }
                // add the student to the arrayList
                this.studentList.add(newStudent);
            }

            // provide option to continue adding students
            System.out.print("Type 'Y' to add another student or 'N' to stop adding students: ");
            addStudent = addStudentInput.nextLine();
            System.out.println();

        }
        // save the added students to a file named "Student_Record.txt"
        saveStudents();

    }

    String getStringInput(String dataType) {
        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "error";

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter " + dataType + ": ");

                newInput = input.nextLine();

                // as long as there was no exception continue to the next input
                continueInput = false;
            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be text characters or numbers.\n");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current string
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }

    String getIdInput() {

        // Create Scanner object for input
        Scanner input = new Scanner(System.in);

        boolean continueInput;
        String newInput = "00000000";

        do {
            // error handling for mismatched input
            try {
                System.out.println("Please enter an 8 digit Student ID: ");

                newInput = input.next();

                int length = String.valueOf(newInput).length();

                if (length != 8) {
                    // keep the loop going and provide an error
                    continueInput = true;
                    System.out.println("Error: The Student ID must be exactly 8 digits.\n");
                }
                else {
                    // exit loop
                    continueInput = false;
                }

                // as long as there was no exception continue to the next input

            }
            // catch any input mismatch errors
            catch (InputMismatchException ex) {
                // display the error message
                System.out.println("Error: The input must be an integer made up of numbers 0-9.\n");
                // clear input
                input.nextLine();
                // make sure the loop will happen again for the current integer
                continueInput = true;
            }
        } while (continueInput);

        return newInput;
    }

    public void saveStudents() {
        // convert the student object to JSON
        for (Object students : this.studentList) {
            save(JSON.studentToJSON((Student)students));
        }
    }

    public void save(String student) {
        // save the JSON to a file named "Student_Record.txt"
        java.io.PrintWriter output = null;
        try{

            // create a new file and open it. Or open existing file.
            this.save = new java.io.File("Student_Record.txt");
            output = new java.io.PrintWriter(new FileWriter(this.save, true));

            // save the data to the file on its own line
            output.print( student + "\n");
        }
        catch (IOException ex) {
            System.out.println("Error: Something went wrong while recording the student data.\n");
        }
        finally {
            // always close the file
            output.close();
        }

    }

    public void displayRecord() {

        String nextLine = "";

        // display all the saved records from the file.
        System.out.println("Saved Student Records:");
        System.out.println();
        try {
            // read each line from the file as a string then convert it from JSON back into a Student Object
            BufferedReader br = new BufferedReader(new FileReader("Student_Record.txt"));
            while (nextLine != null)
            {
                nextLine = br.readLine();
                if (nextLine != null) {

                    //System.out.println(nextLine);
                    this.saveFileList.add(JSON.JSONToStudent(nextLine));
                }
                else {
                    break;
                }
            }
            br.close();

        }
        // handle no file error
        catch (FileNotFoundException e) {
            System.out.println("Error: There was no file found.\n");
        }
        // handle I/O error
        catch (IOException ex){
            System.out.println("Error: Something went wrong while reading the record data.\n");
        }

        // print out the students in a nice format.
        for (Object fileList : this.saveFileList) {
            System.out.println(fileList.toString());
        }
    }
}
