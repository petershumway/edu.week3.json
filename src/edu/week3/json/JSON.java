package edu.week3.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JSON {

    // Member methods
    public static String studentToJSON(Student student) {

        ObjectMapper mapper = new ObjectMapper();
        String studentString = "";

        try {
            studentString = mapper.writeValueAsString(student);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return studentString;
    }

    public static Student JSONToStudent(String studentString) {

        ObjectMapper mapper = new ObjectMapper();
        Student student = null;

        try {
            student = mapper.readValue(studentString, Student.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return student;
    }

}
