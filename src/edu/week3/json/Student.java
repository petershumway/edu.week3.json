package edu.week3.json;

public class Student {
    // Data member variables
    private String name;
    private String parentName;
    private String studentId;
    private String teacher;
    private char grade;

    // Member methods

    // The constructer will fill the data by default to help avoid any errors of missing data
    Student() {
        this.name = "Missing Name";
        this.parentName = "Missing Parent";
        this.studentId = "00000000";
        this.teacher = "Not Assigned";
        this.grade = 'A';
    }

    // getter methods
    public String getName() {
        return this.name;
    }

    public String getParentName() {
        return this.parentName;
    }

    public String getStudentId() {
        return this.studentId;
    }

    public String getTeacher() {
        return this.teacher;
    }

    public char getGrade() {
        return this.grade;
    }

    // setter methods
    public void setName(String name) {
        this.name = name;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public void setGrade(char grade) {
        this.grade = grade;
    }

    public String toString() {
        return "Student Name: " + this.name + "\nParent Name: " + this.parentName + "\nStudent ID: " +
                this.studentId + "\n" + "Teacher Assigned: " + this.teacher + "\nCurrent Grade: " + this.grade + "\n";
    }
}
